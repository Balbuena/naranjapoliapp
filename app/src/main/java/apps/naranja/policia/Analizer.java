package apps.naranja.policia;

import java.util.List;

public class Analizer {
    private List<Result> results;

    public Analizer(List<Result> results) {
        this.results = results;
    }

    public String getStatus() {
        int percent = 0;
        String status = "";
        int prob = 0;

        for(int i = 0;i < results.size();i++) {
            if(results.get(i).getAnger() > 50)
                percent += 30;
            if(results.get(i).getDisgust() > 30)
                percent += 20;
            if(results.get(i).getFear() > 10)
                percent += 30;

            prob += results.get(i).getAnger() + results.get(i).getFear();

            status += results.get(i).getQuestion() + "\n" +
                    "Probabilidad de mentira: " + percent + "\n\n";

            percent = 0;
        }

        if(prob > 80)
            status += "Es probable que este bajo efecto de estupefacientes";
        else
            status += "La persona parece estar normal";

        return status;
    }
}
