package apps.naranja.policia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.affectiva.android.affdex.sdk.Frame;
import com.affectiva.android.affdex.sdk.detector.CameraDetector;
import com.affectiva.android.affdex.sdk.detector.Detector;
import com.affectiva.android.affdex.sdk.detector.Face;
import com.example.policia.R;

import apps.naranja.policia.database.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class AffdexActivity extends AppCompatActivity implements Detector.ImageListener {
    private CameraDetector detector;

    private SurfaceView surface;

    private String quest;
    private double anger;
    private double disgust;
    private double fear;
    private int cont = 1;
    private List<Result> results;

    private TextView question;
    private Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affdex);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        surface = findViewById(R.id.surface);
        question = findViewById(R.id.question);
        next = findViewById(R.id.next);
        results = new ArrayList<>();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                results.add(new Result(quest, anger, disgust, fear));

                if(cont <= 10) {
                    getNextQuestion();

                    if(cont == 10) {
                        next.setText("Terminar test");
                    }

                    cont++;
                }
                else {
                    Analizer analizer = new Analizer(results);
                    Intent intent = new Intent(getApplication(), ResultActivity.class);
                    intent.putExtra("status", analizer.getStatus());
                    startActivity(intent);
                }

            }
        });

        ViewGroup.LayoutParams params = surface.getLayoutParams();
        params.width = (int) (width * 0.45);
        params.height = (int) (height * 0.40);
        surface.setLayoutParams(params);

        detector = new CameraDetector(this, CameraDetector.CameraType.CAMERA_BACK, surface);
        detector.setImageListener(this);
        detector.setDetectAnger(true);
        detector.setDetectDisgust(true);
        detector.setDetectFear(true);
        detector.setMaxProcessRate(10);

        getNextQuestion();
        cont++;
    }

    private void getNextQuestion() {
        try {
            DBHelper helper = new DBHelper(this, "police", null, 1);
            SQLiteDatabase database = helper.getReadableDatabase();

            if(database != null) {
                Cursor cursor = database.rawQuery("SELECT * FROM question WHERE id=" + cont,null);
                if(cursor.moveToFirst()) {
                    question.setText(cursor.getInt(0) + ".- " + cursor.getString(1));
                    quest = cursor.getInt(0) + ".- " + cursor.getString(1);
                }
            }
        }
        catch (Exception e) {
            Log.e("err", e.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            detector.start();
        }
        catch (Exception e) {
            Toast.makeText(this, "Debes dar permisos a la camara", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detector.stop();
    }

    @Override
    public void onImageResults(List<Face> list, Frame frame, float v) {
        if(list.size() > 0) {
            Face face = list.get(0);

            anger = face.emotions.getAnger();
            disgust = face.emotions.getDisgust();
            fear = face.emotions.getFear();

        }
    }
}
